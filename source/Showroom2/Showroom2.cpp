#include "stdafx.h"
#include <stdlib.h> // necesare pentru citirea shaderStencilTesting-elor
#include <stdio.h>
#include <math.h> 

#include <GL/glew.h>

#define GLM_FORCE_CTOR_INIT 
#include <GLM.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <glfw3.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#pragma comment (lib, "glfw3dll.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")
bool pushed = false;
float creste = 0.0;
float crestee = 0.0;
float cresteee = 0.0;

float portbagaj = 0.0f;
float motor = 0.0f;
float usaStanga = 0.0f;
float usaDreapta = 0.0f;
float roti = 0.1f;
float rotiPushed = false;
//colors
int changeGreen = 0;
int changeOrange = 0;
int changeViolet = 0;
int changeWhite = 0;
int changeRed = 0;
int changeDarkBlue = 0;
int changeYellow = 0;

float YupDown = 0.0f;
float XleftRight = 0.0f;
float zUpDown = 0.0f;
float test = 0.0f;
bool testPushed = false;
int choosedCar = 1;
std::string strExePath;

// settings
const unsigned int SCR_WIDTH = 1800;
const unsigned int SCR_HEIGHT = 1200;

struct Material {
	char materialName[30];
	char textureName[200];
	float SpecularWeight;//Ns
	float Opacity; //d
	float Transperency; //Tr
	int illum; //illum
	float ambient[3]; //Ka
	float diffuse[3]; //Kd
	float specular[3]; //Ks
	unsigned int materialId;
	unsigned int materialTextureId;
};
struct Vertex {
	float VertexX;
	float VertexY;
	float VertexZ;
};
struct NormalVertex {
	float VertexNormalX;
	float VertexNormalY;
	float VertexNormalZ;
};
struct TextureVertex {
	float VertexTextureX;
	float VertexTextureY;
	float VertexTextureZ;
};

struct Face {
	int vert;
	int tex;
	int norm;
};
struct Car {
	unsigned int modelVAO = 0;
	unsigned int modelVBO = 0;
	unsigned int modelEBO = 0;

	unsigned int engineVAO = 0;
	unsigned int engineVBO = 0;
	unsigned int engineEBO = 0;

	unsigned int engineRightVAO = 0;
	unsigned int engineRightVBO = 0;
	unsigned int engineRightEBO = 0;

	unsigned int engineLeftVAO = 0;
	unsigned int engineLeftVBO = 0;
	unsigned int engineLeftEBO = 0;

	unsigned int trunkVAO = 0;
	unsigned int trunkVBO = 0;
	unsigned int trunkEBO = 0;

	unsigned int rightDoorVAO = 0;
	unsigned int rightDoorVBO = 0;
	unsigned int rightDoorEBO = 0;

	unsigned int leftDoorVAO = 0;
	unsigned int leftDoorVBO = 0;
	unsigned int leftDoorEBO = 0;

	unsigned int wheelFrontRightVAO = 0;
	unsigned int wheelFrontRightVBO = 0;
	unsigned int wheelFrontRightEBO = 0;

	unsigned int wheelFrontLeftVAO = 0;
	unsigned int wheelFrontLeftVBO = 0;
	unsigned int wheelFrontLeftEBO = 0;

	unsigned int wheelBackRightVAO = 0;
	unsigned int wheelBackRightVBO = 0;
	unsigned int wheelBackRightEBO = 0;

	unsigned int wheelBackLeftVAO = 0;
	unsigned int wheelBackLeftVBO = 0;
	unsigned int wheelBackLeftEBO = 0;
};





char materialName[50];

unsigned int textureId = 0;
unsigned int CreateTexture(const std::string& strTexturePath)
{
	textureId++;
	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	unsigned char *data = stbi_load(strTexturePath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else {
		std::cout << "Failed to load texture:" << strTexturePath << std::endl;
	}
	stbi_image_free(data);

	return textureId;
}
void PopulateMaterials(const char* name , std::vector<Material> &materials) {
	std::ifstream myReadFile;
	myReadFile.open(name);
	char line[100];
	int curMaterial = -1;
	struct Material mat;
	if (myReadFile.is_open()) {
		while (!myReadFile.eof()) {
			myReadFile >> line;
			if (strcmp(line, "newmtl") == 0) {

				if (mat.textureName[0] != 92) {
					strcpy_s(mat.textureName, "NULL");
				}
				if (curMaterial != -1) {
					materials.push_back(mat);
				}
				curMaterial++;
				myReadFile >> mat.materialName;
			}
			else if (line[0] == 'N' && line[1] == 's') {
				myReadFile >> mat.SpecularWeight;
			}
			else if (line[0] == 'd' && line[1] == NULL) {
				myReadFile >> mat.Opacity;
			}
			else if (line[0] == 'T' && line[1] == 'r') {
				myReadFile >> mat.Transperency;
			}
			else if (line[0] == 'i' && line[1] == 'l') {
				myReadFile >> mat.illum;
			}
			else if (line[0] == 'K' && line[1] == 'a') {
				myReadFile >> mat.ambient[0];
				myReadFile >> mat.ambient[1];
				myReadFile >> mat.ambient[2];
			}
			else if (line[0] == 'K' && line[1] == 'd') {
				myReadFile >> mat.diffuse[0];
				myReadFile >> mat.diffuse[1];
				myReadFile >> mat.diffuse[2];
			}
			else if (line[0] == 'K' && line[1] == 's') {
				myReadFile >> mat.specular[0];
				myReadFile >> mat.specular[1];
				myReadFile >> mat.specular[2];
			}
			else if (line[0] == 'm' && line[1] == 'a' && line[2] == 'p') {
				std::string str;
				std::getline(myReadFile, str);
				int pos;
				for (int i = 0; i < str.length(); i++) {
					if (str[i] == 92) {
						pos = i;
					}
				}
				strcpy_s(mat.textureName, str.substr(pos).c_str());
			}
		}
	}
	if (curMaterial != -1) {
		materials.push_back(mat);
	}
	else {
		std::cout << "error, cannot open file";
	}
}
void PopulateMaterialsTexture(std::vector<Material> &materials) {
	for (int i = 0; i < materials.size(); i++) {
		if (materials[i].textureName[0] != 'N' && materials[i].textureName[1] != 'U' && materials[i].textureName[2] != 'L' && materials[i].textureName[3] != 'L') {
			materials[i].materialId = CreateTexture(strExePath + materials[i].textureName);
			materials[i].materialTextureId = materials[i].materialId;
		}
	}
}
void populateVerticesAndIndices(std::string name , std::vector<Vertex> &vertices , std::vector<NormalVertex> &normalVertices , std::vector<TextureVertex> &textureVertices , std::vector<Face> &faces , 
								std::vector<Material> &materials , std::vector<int> &materialIndexPosition , std::vector<int> &materialPosition) {
	char curMaterial[50];
	int materialCount = 0;
	std::ifstream myReadFile;
	myReadFile.open(name);
	char line[100];

	if (myReadFile.is_open()) {
		while (!myReadFile.eof()) {
			myReadFile >> line;
			if (strcmp(line, "mtllib") == 0) {
				myReadFile >> materialName;
				PopulateMaterials(materialName , materials);
				PopulateMaterialsTexture(materials);
			}
			else if (line[0] == 'v' && line[1] == NULL) {
				struct Vertex v;
				myReadFile >> v.VertexX;
				myReadFile >> v.VertexY;
				myReadFile >> v.VertexZ;
				vertices.push_back(v);
			}
			else if (line[0] == 'v' && line[1] == 'n') {
				struct NormalVertex v;
				myReadFile >> v.VertexNormalX;
				myReadFile >> v.VertexNormalY;
				myReadFile >> v.VertexNormalZ;
				normalVertices.push_back(v);
			}
			else if (line[0] == 'v' && line[1] == 't') {
				struct TextureVertex v;
				myReadFile >> v.VertexTextureX;
				myReadFile >> v.VertexTextureY;
				myReadFile >> v.VertexTextureZ;
				textureVertices.push_back(v);

			}
			else if (line[0] == 'f' && line[1] == NULL) {
				struct Face face;
				myReadFile >> line;
				sscanf_s(line, "%d/%d/%d", &face.vert, &face.tex, &face.norm);
				faces.push_back(face);
				myReadFile >> line;
				sscanf_s(line, "%d/%d/%d", &face.vert, &face.tex, &face.norm);
				faces.push_back(face);
				myReadFile >> line;
				sscanf_s(line, "%d/%d/%d", &face.vert, &face.tex, &face.norm);
				faces.push_back(face);
				materialCount += 3;
			}
			else if (strcmp(line, "usemtl") == 0) {
				myReadFile >> curMaterial;
				for (int i = 0; i < materials.size(); i++) {
					if (strcmp(curMaterial, materials[i].materialName) == 0) {
						materialIndexPosition.push_back(materials[i].materialId);
						break;
					}
				}
				materialPosition.push_back(materialCount);
			}
		}
	}
	else {
		std::cout << "error, cannot open file";
		return;
	}
	materialPosition.push_back(faces.size());
}

void GeneratePolygonsArray (std::vector<Vertex> &vertices, std::vector<NormalVertex> &normalVertices, std::vector<TextureVertex> &textureVertices, std::vector<Face> &faces , std::vector<float> &polygons ) {
	for (int i = 0; i < faces.size(); i++) {
		polygons.push_back(vertices[faces[i].vert - 1].VertexX);
		polygons.push_back(vertices[faces[i].vert - 1].VertexY);
		polygons.push_back(vertices[faces[i].vert - 1].VertexZ);
		polygons.push_back(normalVertices[faces[i].norm - 1].VertexNormalX);
		polygons.push_back(normalVertices[faces[i].norm - 1].VertexNormalY);
		polygons.push_back(normalVertices[faces[i].norm - 1].VertexNormalZ);
		polygons.push_back(textureVertices[faces[i].tex - 1].VertexTextureX);
		polygons.push_back(textureVertices[faces[i].tex - 1].VertexTextureY);
		polygons.push_back(textureVertices[faces[i].tex - 1].VertexTextureZ);
	}
}

void PushAndClear(std::vector<Vertex> &vertices, std::vector<NormalVertex> &normalVertices, std::vector<TextureVertex> &textureVertices, std::vector<Face> &faces, 
				  std::vector<int> &materialPosition, std::vector<Material> &materials, std::vector<int> &materialIndexPosition, std::vector<float> &polys, 
				  std::vector<std::vector<Material>> &materialsVector, std::vector<std::vector<int>> &materialPositionVector, std::vector<std::vector<int>> &materialIndexPositonVector, std::vector<std::vector<float>> &polysVector) {
	materialsVector.push_back(materials);
	materialPositionVector.push_back(materialPosition);
	materialIndexPositonVector.push_back(materialIndexPosition);
	polysVector.push_back(polys);
	materials.clear();
	polys.clear();
	materialIndexPosition.clear();
	materialPosition.clear();
	vertices.clear();
	normalVertices.clear();
	textureVertices.clear();
	faces.clear();
}
enum ECameraMovementType
{
	UNKNOWN,
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

class Camera
{
private:
	// Default camera values
	const float zNEAR = 0.1f;
	const float zFAR = 500.f;
	const float YAW = -90.0f;
	const float PITCH = 0.0f;
	const float FOV = 45.0f;
	glm::vec3 startPosition;

public:
	Camera(const int width, const int height, const glm::vec3 &position)
	{
		startPosition = position;
		Set(width, height, position);
	}

	void Set(const int width, const int height, const glm::vec3 &position)
	{
		this->isPerspective = true;
		this->yaw = YAW;
		this->pitch = PITCH;

		this->FoVy = FOV;
		this->width = width;
		this->height = height;
		this->zNear = zNEAR;
		this->zFar = zFAR;

		this->worldUp = glm::vec3(0, 1, 0);
		this->position = position;

		lastX = width / 2.0f;
		lastY = height / 2.0f;
		bFirstMouseMove = true;

		UpdateCameraVectors();
	}

	void Reset(const int width, const int height)
	{
		Set(width, height, startPosition);
	}

	void Reshape(int windowWidth, int windowHeight)
	{
		width = windowWidth;
		height = windowHeight;

		// define the viewport transformation
		glViewport(0, 0, windowWidth, windowHeight);
	}

	const glm::vec3 GetPosition() const
	{
		return position;
	}

	const glm::mat4 GetViewMatrix() const
	{
		// Returns the View Matrix
		return glm::lookAt(position, position + forward, up);
	}

	const glm::mat4 GetProjectionMatrix() const
	{
		glm::mat4 Proj = glm::mat4(1);
		if (isPerspective) {
			float aspectRatio = ((float)(width)) / height;
			Proj = glm::perspective(glm::radians(FoVy), aspectRatio, zNear, zFar);
		}
		else {
			float scaleFactor = 2000.f;
			Proj = glm::ortho<float>(
				-width / scaleFactor, width / scaleFactor,
				-height / scaleFactor, height / scaleFactor, -zFar, zFar);
		}
		return Proj;
	}

	void ProcessKeyboard(ECameraMovementType direction, float deltaTime)
	{
		float velocity = (float)(cameraSpeedFactor * deltaTime);
		switch (direction) {
		case ECameraMovementType::FORWARD:
			position += forward * velocity;
			break;
		case ECameraMovementType::BACKWARD:
			position -= forward * velocity;
			break;
		case ECameraMovementType::LEFT:
			position -= right * velocity;
			break;
		case ECameraMovementType::RIGHT:
			position += right * velocity;
			break;
		case ECameraMovementType::UP:
			position += up * velocity;
			break;
		case ECameraMovementType::DOWN:
			position -= up * velocity;
			break;
		}
	}

	void MouseControl(float xPos, float yPos)
	{
		if (bFirstMouseMove) {
			lastX = xPos;
			lastY = yPos;
			bFirstMouseMove = false;
		}

		float xChange = xPos - lastX;
		float yChange = lastY - yPos;
		lastX = xPos;
		lastY = yPos;

		if (fabs(xChange) <= 1e-6 && fabs(yChange) <= 1e-6) {
			return;
		}
		xChange *= mouseSensitivity;
		yChange *= mouseSensitivity;

		ProcessMouseMovement(xChange, yChange);
	}

	void ProcessMouseScroll(float yOffset)
	{
		if (FoVy >= 1.0f && FoVy <= 90.0f) {
			FoVy -= yOffset;
		}
		if (FoVy <= 1.0f)
			FoVy = 1.0f;
		if (FoVy >= 90.0f)
			FoVy = 90.0f;
	}

private:
	void ProcessMouseMovement(float xOffset, float yOffset, bool constrainPitch = true)
	{
		yaw += xOffset;
		pitch += yOffset;

		//std::cout << "yaw = " << yaw << std::endl;
		//std::cout << "pitch = " << pitch << std::endl;

		// Avem grijã sã nu ne dãm peste cap
		if (constrainPitch) {
			if (pitch > 89.0f)
				pitch = 89.0f;
			if (pitch < -89.0f)
				pitch = -89.0f;
		}

		// Se modificã vectorii camerei pe baza unghiurilor Euler
		UpdateCameraVectors();
	}

	void UpdateCameraVectors()
	{
		// Calculate the new forward vector
		this->forward.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		this->forward.y = sin(glm::radians(pitch));
		this->forward.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		this->forward = glm::normalize(this->forward);
		// Also re-calculate the Right and Up vector
		right = glm::normalize(glm::cross(forward, worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		up = glm::normalize(glm::cross(right, forward));
	}

protected:
	const float cameraSpeedFactor = 2.5f;
	const float mouseSensitivity = 0.1f;

	// Perspective properties
	float zNear;
	float zFar;
	float FoVy;
	int width;
	int height;
	bool isPerspective;

	glm::vec3 position;
	glm::vec3 forward;
	glm::vec3 right;
	glm::vec3 up;
	glm::vec3 worldUp;

	// Euler Angles
	float yaw;
	float pitch;

	bool bFirstMouseMove = true;
	float lastX = 0.f, lastY = 0.f;
};

class Shader
{
public:
	// constructor generates the shaderStencilTesting on the fly
	// ------------------------------------------------------------------------
	Shader(const char* vertexPath, const char* fragmentPath)
	{
		Init(vertexPath, fragmentPath);
	}

	~Shader()
	{
		glDeleteProgram(ID);
	}

	// activate the shaderStencilTesting
	// ------------------------------------------------------------------------
	void Use() const
	{
		glUseProgram(ID);
	}

	unsigned int GetID() const { return ID; }

	// MVP
	unsigned int loc_model_matrix;
	unsigned int loc_view_matrix;
	unsigned int loc_projection_matrix;

	// utility uniform functions
	void SetInt(const std::string &name, int value) const
	{
		glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
	}
	void SetFloat(const std::string &name, float value) const
	{
		glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
	}
	void SetVec3(const std::string &name, const glm::vec3 &value) const
	{
		glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
	}
	void SetVec3(const std::string &name, float x, float y, float z) const
	{
		glUniform3f(glGetUniformLocation(ID, name.c_str()), x, y, z);
	}
	void SetMat4(const std::string &name, const glm::mat4 &mat) const
	{
		glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
	}

private:
	void Init(const char* vertexPath, const char* fragmentPath)
	{
		// 1. retrieve the vertex/fragment source code from filePath
		std::string vertexCode;
		std::string fragmentCode;
		std::ifstream vShaderFile;
		std::ifstream fShaderFile;
		// ensure ifstream objects can throw exceptions:
		vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		try {
			// open files
			vShaderFile.open(vertexPath);
			fShaderFile.open(fragmentPath);
			std::stringstream vShaderStream, fShaderStream;
			// read file's buffer contents into streams
			vShaderStream << vShaderFile.rdbuf();
			fShaderStream << fShaderFile.rdbuf();
			// close file handlers
			vShaderFile.close();
			fShaderFile.close();
			// convert stream into string
			vertexCode = vShaderStream.str();
			fragmentCode = fShaderStream.str();
		}
		catch (std::ifstream::failure e) {
			std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
		}
		const char* vShaderCode = vertexCode.c_str();
		const char * fShaderCode = fragmentCode.c_str();

		// 2. compile shaders
		unsigned int vertex, fragment;
		// vertex shaderStencilTesting
		vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &vShaderCode, NULL);
		glCompileShader(vertex);
		CheckCompileErrors(vertex, "VERTEX");
		// fragment Shader
		fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment, 1, &fShaderCode, NULL);
		glCompileShader(fragment);
		CheckCompileErrors(fragment, "FRAGMENT");
		// shaderStencilTesting Program
		ID = glCreateProgram();
		glAttachShader(ID, vertex);
		glAttachShader(ID, fragment);
		glLinkProgram(ID);
		CheckCompileErrors(ID, "PROGRAM");

		// 3. delete the shaders as they're linked into our program now and no longer necessery
		glDeleteShader(vertex);
		glDeleteShader(fragment);
	}

	// utility function for checking shaderStencilTesting compilation/linking errors.
	// ------------------------------------------------------------------------
	void CheckCompileErrors(unsigned int shaderStencilTesting, std::string type)
	{
		GLint success;
		GLchar infoLog[1024];
		if (type != "PROGRAM") {
			glGetShaderiv(shaderStencilTesting, GL_COMPILE_STATUS, &success);
			if (!success) {
				glGetShaderInfoLog(shaderStencilTesting, 1024, NULL, infoLog);
				std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
			}
		}
		else {
			glGetProgramiv(shaderStencilTesting, GL_LINK_STATUS, &success);
			if (!success) {
				glGetProgramInfoLog(shaderStencilTesting, 1024, NULL, infoLog);
				std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
			}
		}
	}
private:
	unsigned int ID;
};

Camera *pCamera = nullptr;



void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

void renderScene(const Shader &shader, std::vector<std::vector<Material>> &materialsVector, std::vector<std::vector<int>> &materialPositionVector, std::vector<std::vector<int>> &materialIndexPositonVector, std::vector<std::vector<float>> &polysVector , std::vector<Car> &car);
void render(std::vector<int> &materialPosition, std::vector<Material> &materials, std::vector<int> &materialIndexPosition, std::vector<float> &polys, unsigned int *VAO, unsigned int *VBO, unsigned int *EBO);
void ChangeColor(std::vector<std::vector<Material>> &materialsVector, unsigned int &color, unsigned int &number);
void renderFloor();

// timing
double deltaTime = 0.0f;    // time between current frame and last frame
double lastFrame = 0.0f;

int main(int argc, char** argv)
{

	std::string strFullExeFileName = argv[0];

	const size_t last_slash_idx = strFullExeFileName.rfind('\\');
	if (std::string::npos != last_slash_idx) {
		strExePath = strFullExeFileName.substr(0, last_slash_idx);
	}

	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Lab8 - Maparea umbrelor", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewInit();

	// Create camera
	pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(0.0, 1.0, 3.0));

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// build and compile shaders
	// -------------------------
	Shader shadowMappingShader("ShadowMapping.vs", "ShadowMapping.fs");
	Shader shadowMappingDepthShader("ShadowMappingDepth.vs", "ShadowMappingDepth.fs");


	std::vector<Vertex> vertices;
	std::vector<NormalVertex> normalVertices;
	std::vector<TextureVertex> textureVertices;
	std::vector<Face> faces;


	std::vector<Material> materials;
	std::vector<int> materialPosition;
	std::vector<int> materialIndexPosition;
	std::vector<float> polygons;


	std::vector<std::vector<Material>> materialsVector;
	std::vector<std::vector<int>> materialPositionVector;
	std::vector<std::vector<int>> materialIndexPositonVector;
	std::vector<std::vector<float>> polysVector;
	
	std::vector<Car> car;
	Car automobile;
	car.push_back(automobile);
	car.push_back(automobile);
	car.push_back(automobile);
	unsigned int floorTexture = CreateTexture(strExePath + "\\floor.png");
	char lambo[6] = "Lambo";
	char camaro[7] = "Camaro";
	char classicCamaro[14] = "ClassicCamaro";
	char modl[10] = "Model.obj";
	char engine[15] = "EngineHood.obj";
	char trunk[14] = "TrunkHood.obj";
	char rightDoor[14] = "DoorRight.obj";
	char leftDoor[13] = "DoorLeft.obj";
	char wheelFrontRight[20] = "WheelFrontRight.obj";
	char wheelFrontLeft[19] = "WheelFrontLeft.obj";
	char wheelBackRight[19] = "WheelBackRight.obj";
	char wheelBackLeft[18] = "WheelBackLeft.obj";
	std::vector<char*> cars;
	std::vector<char*> components;
	cars.push_back(&lambo[0]);
	cars.push_back(&camaro[0]);
	cars.push_back(&classicCamaro[0]);
	components.push_back(&modl[0]);
	components.push_back(&engine[0]);
	components.push_back(&trunk[0]);
	components.push_back(&rightDoor[0]);
	components.push_back(&leftDoor[0]);
	components.push_back(&wheelFrontRight[0]);
	components.push_back(&wheelFrontLeft[0]);
	components.push_back(&wheelBackRight[0]);
	components.push_back(&wheelBackLeft[0]);
	for (int j = 0; j < cars.size(); j++) {
		for (int i = 0; i < components.size(); i++) {
			std::string name = cars[j];
			name += components[i];
			populateVerticesAndIndices(name, vertices, normalVertices, textureVertices, faces, materials, materialIndexPosition, materialPosition);
			if (vertices.size() > 0) {
				GeneratePolygonsArray(vertices, normalVertices, textureVertices, faces, polygons);
				PushAndClear(vertices, normalVertices, textureVertices, faces, materialPosition, materials, materialIndexPosition, polygons, materialsVector, materialPositionVector, materialIndexPositonVector, polysVector);
			}
		}
	}
	populateVerticesAndIndices("ClassicCamaroEngineHoodRight.obj", vertices, normalVertices, textureVertices, faces, materials, materialIndexPosition, materialPosition);
	GeneratePolygonsArray(vertices, normalVertices, textureVertices, faces, polygons);
	PushAndClear(vertices, normalVertices, textureVertices, faces, materialPosition, materials, materialIndexPosition, polygons, materialsVector, materialPositionVector, materialIndexPositonVector, polysVector);

	populateVerticesAndIndices("ClassicCamaroEngineHoodLeft.obj", vertices, normalVertices, textureVertices, faces, materials, materialIndexPosition, materialPosition);
	GeneratePolygonsArray(vertices, normalVertices, textureVertices, faces, polygons);
	PushAndClear(vertices, normalVertices, textureVertices, faces, materialPosition, materials, materialIndexPosition, polygons, materialsVector, materialPositionVector, materialIndexPositonVector, polysVector);

	unsigned int colorNumber = 0;
	for (int i = 0; i < materialsVector.size(); i++) {
		for (int j = 0; j < materialsVector[i].size(); j++) {
			if (materialsVector[i][j].textureName[1] == 'O' && materialsVector[i][j].textureName[2] == 'r' && materialsVector[i][j].textureName[3] == 'a' && materialsVector[i][j].textureName[4] == 'n' && materialsVector[i][j].textureName[5] == 'g' && materialsVector[i][j].textureName[6] == 'e')
			{
				if (colorNumber == 0)
					colorNumber = materialsVector[i][j].materialTextureId;
				else {
					glDeleteTextures(1, &materialsVector[i][j].materialTextureId);
					materialsVector[i][j].materialTextureId = colorNumber;
				}
			}
		}
	}
	unsigned int greenId = CreateTexture(strExePath + "\\Green.png");
	unsigned int orangeId = colorNumber;
	unsigned int redId = CreateTexture(strExePath + "\\Red.png");
	unsigned int violetId = CreateTexture(strExePath + "\\Violet.png");
	unsigned int whiteId = CreateTexture(strExePath + "\\White.png");
	unsigned int darkBlueId = CreateTexture(strExePath + "\\DarkBlue.png");
	unsigned int yellowId = CreateTexture(strExePath + "\\Yellow.png");
	
	// load textures
	// -------------
	// configure depth map FBO
	// -----------------------
	const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);
	// create depth texture
	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	// shader configuration
	// --------------------
	shadowMappingShader.Use();
	shadowMappingShader.SetInt("diffuseTexture", 0);
	shadowMappingShader.SetInt("shadowMap", 1);

	// lighting info
	// -------------
	glm::vec3 lightPos(-2.0f, 4.0f, -1.0f);

	glEnable(GL_CULL_FACE);

	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		float currentFrame = (float)glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		if (changeGreen == 1) {
			ChangeColor(materialsVector, greenId, colorNumber);
			changeGreen++;
		}
		else if (changeOrange == 1) {
			ChangeColor(materialsVector, orangeId, colorNumber);
			changeOrange++;
		}
		else if (changeRed == 1) {
			ChangeColor(materialsVector, redId, colorNumber);
			changeRed++;
		}
		else if (changeWhite == 1) {
			ChangeColor(materialsVector, whiteId, colorNumber);
			changeWhite++;
		}
		else if (changeViolet == 1) {
			ChangeColor(materialsVector, violetId, colorNumber);
			changeViolet++;
		}
		else if (changeYellow == 1) {
			ChangeColor(materialsVector, yellowId, colorNumber);
			changeYellow++;
		}
		else if (changeDarkBlue == 1) {
			ChangeColor(materialsVector, darkBlueId, colorNumber);
			changeDarkBlue++;
		}
		if (testPushed == true)
			test += 2 * deltaTime;

		if (rotiPushed) {
			roti += 10 * deltaTime;
		}
		// input
		// -----
		processInput(window);
		if (pushed == true)

			// render
			// ------
			glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// 1. render depth of scene to texture (from light's perspective)
		glm::mat4 lightProjection, lightView;
		glm::mat4 lightSpaceMatrix;
		float near_plane = 1.0f, far_plane = 7.5f;
		lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
		lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
		lightSpaceMatrix = lightProjection * lightView;

		// render scene from light's point of view
		shadowMappingDepthShader.Use();
		shadowMappingDepthShader.SetMat4("lightSpaceMatrix", lightSpaceMatrix);

		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		renderScene(shadowMappingDepthShader, materialsVector , materialPositionVector , materialIndexPositonVector , polysVector ,car);
		glCullFace(GL_BACK);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// reset viewport
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// 2. render scene as normal using the generated depth/shadow map 
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		shadowMappingShader.Use();
		glm::mat4 projection = pCamera->GetProjectionMatrix();
		glm::mat4 view = pCamera->GetViewMatrix();
		shadowMappingShader.SetMat4("projection", projection);
		shadowMappingShader.SetMat4("view", view);
		// set light uniforms
		shadowMappingShader.SetVec3("viewPos", pCamera->GetPosition());
		shadowMappingShader.SetVec3("lightPos", lightPos);
		shadowMappingShader.SetMat4("lightSpaceMatrix", lightSpaceMatrix);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		glDisable(GL_CULL_FACE);
		renderScene(shadowMappingShader, materialsVector, materialPositionVector, materialIndexPositonVector, polysVector , car);

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	delete pCamera;

	glfwTerminate();
	return 0;
}
void ChangeColor(std::vector<std::vector<Material>> &materialsVector, unsigned int &color , unsigned int &number) {
	for (int i = 0; i < materialsVector.size(); i++) {
		for (int j = 0; j < materialsVector[i].size(); j++) {
			if (materialsVector[i][j].materialTextureId == number)
				materialsVector[i][j].materialTextureId = color;
		}
	}
	number = color;
}
// renders the 3D scene
// --------------------
void renderScene(const Shader &shader, std::vector<std::vector<Material>> &materialsVector, std::vector<std::vector<int>> &materialPositionVector, std::vector<std::vector<int>> &materialIndexPositonVector , std::vector<std::vector<float>> &polysVector , std::vector<Car> &car)
{
	//model = glm::translate(model, glm::vec3(0.0f +zUpDown*10, 0.0f+YupDown*10, 0.0f+XleftRight*10));
	//model = glm::rotate(model, 0.0f + test*10, glm::vec3(0.0f, 0.005f, 0.0f));
	//model = glm::translate(model, glm::vec3(0.0f - zUpDown*10, 0.0f - YupDown*10, 0.0f - XleftRight*10));

	// floor
	glm::mat4 model;
	shader.SetMat4("model", model);
	renderFloor();
	//Lamborghini
	if (choosedCar == 1) { //Lamborghini
		//carModel
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.2f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		shader.SetMat4("model", model);
		render(materialPositionVector[0], materialsVector[0], materialIndexPositonVector[0], polysVector[0], &car[0].modelVAO, &car[0].modelVBO, &car[0].modelEBO);
		//carEngineHood
		model = glm::mat4();
		model = glm::scale(model, glm::vec3(0.2f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.170f, 2.357f, -2.860f));
		model = glm::rotate(model, 0.0f + motor, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.170f, -2.357f, 2.860f));
		shader.SetMat4("model", model);
		render(materialPositionVector[1], materialsVector[1], materialIndexPositonVector[1], polysVector[1], &car[0].engineVAO, &car[0].engineVBO, &car[0].engineEBO);
		//carTrunkHood
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.2f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.003f, 2.394f, 3.375f));
		model = glm::rotate(model, 0.0f + portbagaj, glm::vec3(-0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.003f, -2.394f, -3.375f));
		shader.SetMat4("model", model);
		render(materialPositionVector[2], materialsVector[2], materialIndexPositonVector[2], polysVector[2], &car[0].trunkVAO, &car[0].trunkVBO, &car[0].trunkEBO);
		//carRightDoor
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.2f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(-2.7409, 0.619123, 2.16781));
		model = glm::rotate(model, 0.0f + usaDreapta, glm::vec3(0.0f, 0.005f, 0.0f));//DoorRotation
		model = glm::translate(model, glm::vec3(2.7409, -0.619123, -2.16781));
		shader.SetMat4("model", model);
		render(materialPositionVector[3], materialsVector[3], materialIndexPositonVector[3], polysVector[3], &car[0].rightDoorVAO, &car[0].rightDoorVBO, &car[0].rightDoorEBO);
		//carLeftDoor
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.2f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(2.7409, 0.619123, 2.16781));
		model = glm::rotate(model, 0.0f + usaStanga, glm::vec3(0.0f, -0.005f, 0.0f));//DoorRotation
		model = glm::translate(model, glm::vec3(-2.7409, -0.619123, -2.16781));
		shader.SetMat4("model", model);
		render(materialPositionVector[4], materialsVector[4], materialIndexPositonVector[4], polysVector[4], &car[0].leftDoorVAO, &car[0].leftDoorVBO, &car[0].leftDoorEBO);
		//carWHeelFronRight
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.2f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.0f, 1.069f - 0.0238, 3.416f - 0.0339));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));//wheelRotation
		model = glm::translate(model, glm::vec3(0.0f, -1.069f + 0.0238, -3.416f + 0.0339));
		shader.SetMat4("model", model);
		render(materialPositionVector[5], materialsVector[5], materialIndexPositonVector[5], polysVector[5], &car[0].wheelFrontRightVAO, &car[0].wheelFrontRightVBO, &car[0].wheelFrontRightEBO);
		//carWheelFrontLeft
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.2f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.0f, 1.069f - 0.0238, 3.416f - 0.0339));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));//wheelRotation
		model = glm::translate(model, glm::vec3(0.0f, -1.069f + 0.0238, -3.416f + 0.0339));
		shader.SetMat4("model", model);
		render(materialPositionVector[6], materialsVector[6], materialIndexPositonVector[6], polysVector[6], &car[0].wheelFrontLeftVAO, &car[0].wheelFrontLeftVBO, &car[0].wheelFrontLeftEBO);
		//carWheelBackRight
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.2f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.0f, 1.04422, -4.27112));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));//wheelRotation
		model = glm::translate(model, glm::vec3(0.0f, -1.04422, 4.27112));
		shader.SetMat4("model", model);
		render(materialPositionVector[7], materialsVector[7], materialIndexPositonVector[7], polysVector[7], &car[0].wheelBackRightVAO, &car[0].wheelBackRightVBO, &car[0].wheelBackRightEBO);
		//carWheelBackLeft
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.2f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.0f, 1.04422, -4.27112));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));//wheelRotation
		model = glm::translate(model, glm::vec3(0.0f, -1.04422, 4.27112));
		shader.SetMat4("model", model);
		render(materialPositionVector[8], materialsVector[8], materialIndexPositonVector[8], polysVector[8], &car[0].wheelBackLeftVAO, &car[0].wheelBackLeftVBO, &car[0].wheelBackLeftEBO);
	}
	//Camaro
	else if (choosedCar == 2) {//Camaro
		//CarModel
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.05f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		shader.SetMat4("model", model);
		render(materialPositionVector[9], materialsVector[9], materialIndexPositonVector[9], polysVector[9], &car[1].modelVAO, &car[1].modelVBO, &car[1].modelEBO);
		//EngineHood
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.05f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.0f, 12.07, 9.16));
		model = glm::rotate(model, 0.0f + motor, glm::vec3(-0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -12.07,-9.16));
		shader.SetMat4("model", model);
		render(materialPositionVector[10], materialsVector[10], materialIndexPositonVector[10], polysVector[10], &car[1].engineVAO, &car[1].engineVBO, &car[1].engineEBO);
		//TrunkHood
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.05f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.0f, 13.086, -23.333));
		model = glm::rotate(model, 0.0f + portbagaj, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -13.086, 23.333));
		shader.SetMat4("model", model);
		render(materialPositionVector[11], materialsVector[11], materialIndexPositonVector[11], polysVector[11], &car[1].trunkVAO, &car[1].trunkVBO, &car[1].trunkEBO);
		//RightDoor
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.05f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(-11.58 , 0.0f, 7.75));
		model = glm::rotate(model, 0.0f + usaDreapta, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(11.58, 0.0f, -7.75));
		shader.SetMat4("model", model);
		render(materialPositionVector[12], materialsVector[12], materialIndexPositonVector[12], polysVector[12], &car[1].rightDoorVAO, &car[1].rightDoorVBO, &car[1].rightDoorEBO);
		//LeftDoor
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.05f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(11.58, 0.0f, 7.75));
		model = glm::rotate(model, 0.0f + usaStanga, glm::vec3(0.0f, -0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-11.58, 0.0f, -7.75));
		shader.SetMat4("model", model);
		render(materialPositionVector[13], materialsVector[13], materialIndexPositonVector[13], polysVector[13], &car[1].leftDoorVAO, &car[1].leftDoorVBO, &car[1].leftDoorEBO);
		//FrontWheelRight
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.05f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.0f, 4.55, 17.76));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -4.55, -17.76));
		shader.SetMat4("model", model);
		render(materialPositionVector[14], materialsVector[14], materialIndexPositonVector[14], polysVector[14], &car[1].wheelFrontRightVAO, &car[1].wheelFrontRightVBO, &car[1].wheelFrontRightEBO);
		//FrontWheelLeft
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.05f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.0f, 4.55, 17.76));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -4.55, -17.76));
		shader.SetMat4("model", model);
		render(materialPositionVector[15], materialsVector[15], materialIndexPositonVector[15], polysVector[15], &car[1].wheelFrontLeftVAO, &car[1].wheelFrontLeftVBO, &car[1].wheelFrontLeftEBO);
		//BackWheelRight
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.05f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.0f, 4.8, -17.67));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -4.8, 17.67));
		shader.SetMat4("model", model);
		render(materialPositionVector[16], materialsVector[16], materialIndexPositonVector[16], polysVector[16], &car[1].wheelBackRightVAO, &car[1].wheelBackRightVBO, &car[1].wheelBackRightEBO);
		//BackWheelLeft
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.05f));
		model = glm::translate(model, glm::vec3(0.002f, -2.6f, -2.9f));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(-0.002f, 2.6f, 2.9f));
		model = glm::translate(model, glm::vec3(0.0f, 4.8, -17.67));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -4.8, 17.67));
		shader.SetMat4("model", model);
		render(materialPositionVector[17], materialsVector[17], materialIndexPositonVector[17], polysVector[17], &car[1].wheelBackLeftVAO, &car[1].wheelBackLeftVBO, &car[1].wheelBackLeftEBO);
	}
	//ClassicCamaro
	else if (choosedCar == 3) {//ClassicCamaro
		//CarModel
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(1.37, -0.245, -4.05));
		model = glm::scale(model, glm::vec3(0.35f));
		model = glm::translate(model, glm::vec3(-4.34 , 0.0f , 9.4));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(4.34, 0.0f, -9.4));
		shader.SetMat4("model", model);
		render(materialPositionVector[18], materialsVector[18], materialIndexPositonVector[18], polysVector[18], &car[2].modelVAO, &car[2].modelVBO, &car[2].modelEBO);
		//EngineHoodLeft
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(1.37, -0.245, -4.05));
		model = glm::scale(model, glm::vec3(0.35f));
		model = glm::translate(model, glm::vec3(-4.34, 0.0f, 9.4));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(4.34, 0.0f, -9.4));
		model = glm::translate(model, glm::vec3(-2.7, 1.9, 0.0f));
		model = glm::rotate(model, 0.0f + motor, glm::vec3(0.0f, 0.0f, -0.005f));
		model = glm::translate(model, glm::vec3(2.7, -1.9, 0.0f));
		shader.SetMat4("model", model);
		render(materialPositionVector[27], materialsVector[27], materialIndexPositonVector[27], polysVector[27], &car[2].engineLeftVAO, &car[2].engineLeftVBO, &car[2].engineLeftEBO);
		//EngineHoodRight
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(1.37, -0.245, -4.05));
		model = glm::scale(model, glm::vec3(0.35f));
		model = glm::translate(model, glm::vec3(-4.34, 0.0f, 9.4));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(4.34, 0.0f, -9.4));
		model = glm::translate(model, glm::vec3(-5.6, 1.9, 0.0f));
		model = glm::rotate(model, 0.0f + motor, glm::vec3(0.0f, 0.0f, 0.005f));
		model = glm::translate(model, glm::vec3(5.6, -1.9, 0.0f));
		shader.SetMat4("model", model);
		render(materialPositionVector[26], materialsVector[26], materialIndexPositonVector[26], polysVector[26], &car[2].engineRightVAO, &car[2].engineRightVBO, &car[2].engineRightEBO);
		//TrunkHood
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(1.37, -0.245, -4.05));
		model = glm::scale(model, glm::vec3(0.35f));
		model = glm::translate(model, glm::vec3(-4.34, 0.0f, 9.4));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(4.34, 0.0f, -9.4));
		model = glm::translate(model, glm::vec3(0.0f,2.11, 6.16));
		model = glm::rotate(model, 0.0f + portbagaj, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -2.11, -6.16));
		shader.SetMat4("model", model);
		render(materialPositionVector[19], materialsVector[19], materialIndexPositonVector[19], polysVector[19], &car[2].trunkVAO, &car[2].trunkVBO, &car[2].trunkEBO);
		//RightDoor
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(1.37, -0.245, -4.05));
		model = glm::scale(model, glm::vec3(0.35f));
		model = glm::translate(model, glm::vec3(-4.34, 0.0f, 9.4));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(4.34, 0.0f, -9.4));
		model = glm::translate(model, glm::vec3(-6.06, 0.0f, 11.03));
		model = glm::rotate(model, 0.0f + usaDreapta, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(6.06, 0.0f, -11.03));
		shader.SetMat4("model", model);
		render(materialPositionVector[20], materialsVector[20], materialIndexPositonVector[20], polysVector[20], &car[2].rightDoorVAO, &car[2].rightDoorVBO, &car[2].rightDoorEBO);
		//LeftDoor
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(1.37, -0.245, -4.05));
		model = glm::scale(model, glm::vec3(0.35f));
		model = glm::translate(model, glm::vec3(-4.34, 0.0f, 9.4));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(4.34, 0.0f, -9.4));
		model = glm::translate(model, glm::vec3(-2.25, 0.0f, 11.07));
		model = glm::rotate(model, 0.0f + usaStanga, glm::vec3(0.0f, -0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(2.25, 0.0f, -11.07));
		shader.SetMat4("model", model);
		render(materialPositionVector[21], materialsVector[21], materialIndexPositonVector[21], polysVector[21], &car[2].leftDoorVAO, &car[2].leftDoorVBO, &car[2].leftDoorEBO);
		//FrontWheelRight
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(1.37, -0.245, -4.05));
		model = glm::scale(model, glm::vec3(0.35f));
		model = glm::translate(model, glm::vec3(-4.34, 0.0f, 9.4));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(4.34, 0.0f, -9.4));
		model = glm::translate(model, glm::vec3(0.0f  , 0.711, 12.721));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -0.711, -12.721));
		shader.SetMat4("model", model);
		render(materialPositionVector[22], materialsVector[22], materialIndexPositonVector[22], polysVector[22], &car[2].wheelFrontRightVAO, &car[2].wheelFrontRightVBO, &car[2].wheelFrontRightEBO);
		//FrontWheelLeft
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(1.37, -0.245, -4.05));
		model = glm::scale(model, glm::vec3(0.35f));
		model = glm::translate(model, glm::vec3(-4.34, 0.0f, 9.4));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(4.34, 0.0f, -9.4));
		model = glm::translate(model, glm::vec3(0.0f, 0.711, 12.721));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -0.711, -12.721));
		shader.SetMat4("model", model);
		render(materialPositionVector[23], materialsVector[23], materialIndexPositonVector[23], polysVector[23], &car[2].wheelFrontLeftVAO, &car[2].wheelFrontLeftVBO, &car[2].wheelFrontLeftEBO);
		//BackWheelRight
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(1.37, -0.245, -4.05));
		model = glm::scale(model, glm::vec3(0.35f));
		model = glm::translate(model, glm::vec3(-4.34, 0.0f, 9.4));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(4.34, 0.0f, -9.4));
		model = glm::translate(model, glm::vec3(0.0f , 0.772f, 6.761f));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -0.772f, -6.761f));
		shader.SetMat4("model", model);
		render(materialPositionVector[24], materialsVector[24], materialIndexPositonVector[24], polysVector[24], &car[2].wheelBackRightVAO, &car[2].wheelBackRightVBO, &car[2].wheelBackRightEBO);
		//BackWheelLeft
		model = glm::mat4();
		model = glm::translate(model, glm::vec3(1.37, -0.245, -4.05));
		model = glm::scale(model, glm::vec3(0.35f));
		model = glm::translate(model, glm::vec3(-4.34, 0.0f, 9.4));
		model = glm::rotate(model, 0.0f + creste, glm::vec3(0.0f, 0.005f, 0.0f));
		model = glm::translate(model, glm::vec3(4.34, 0.0f, -9.4));
		model = glm::translate(model, glm::vec3(0.0f, 0.772f, 6.761f));
		model = glm::rotate(model, 0.0f + roti, glm::vec3(0.005f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, -0.772f, -6.761f));
		shader.SetMat4("model", model);
		render(materialPositionVector[25], materialsVector[25], materialIndexPositonVector[25], polysVector[25], &car[2].wheelBackLeftVAO, &car[2].wheelBackLeftVBO, &car[2].wheelBackLeftEBO);
	}
}


unsigned int planeVAO = 0;
void renderFloor()
{
	unsigned int planeVBO;

	if (planeVAO == 0) {
		// set up vertex data (and buffer(s)) and configure vertex attributes
		float planeVertices[] = {
			// positions            // normals         // texcoords
			25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,0.0f,
			-25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,0.0f,
			-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,0.0f,

			25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,0.0f,
			-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,0.0f,
			25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f,0.0f
		};
		// plane VAO
		glGenVertexArrays(1, &planeVAO);
		glGenBuffers(1, &planeVBO);
		glBindVertexArray(planeVAO);
		glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindVertexArray(0);
	}

	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}


void render( std::vector<int> &materialPosition , std::vector<Material> &materials , std::vector<int> &materialIndexPosition , std::vector<float> &polys, unsigned int *VAO , unsigned int *VBO , unsigned int *EBO)
{
	
	int iterationsCount = 0;
	unsigned int indicesSplit[21276];
	// initialize (if necessary)
	for (int k = 0; k < materialPosition.size() - 1; k++) {
		for (int i = materialPosition[k]; i < materialPosition[k + 1]; i++) {
			indicesSplit[iterationsCount] = i;
			iterationsCount++;
		}
		iterationsCount = 0;
		if (*VAO == 0)
		{
			glGenVertexArrays(1, VAO);
			glGenBuffers(1, VBO);
			glGenBuffers(1, EBO);
			// fill buffer
			glBindBuffer(GL_ARRAY_BUFFER, *VBO);
			glBufferData(GL_ARRAY_BUFFER, polys.size() * sizeof(float), &polys[0], GL_STATIC_DRAW);
			// link vertex attributes
			glBindVertexArray(*VAO);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));
			glEnableVertexAttribArray(2);
			glBindVertexArray(0);
		}

		for (int i = 0; i < materials.size(); i++) {
			if (materials[i].materialId == materialIndexPosition[k]) {
				glBindTexture(GL_TEXTURE_2D, materials[i].materialTextureId);
				float ambient[] = { materials[i].ambient[0] , materials[i].ambient[1] , materials[i].ambient[2] ,materials[i].Opacity };
				float diffuse[] = { materials[i].diffuse[0] , materials[i].diffuse[1] , materials[i].diffuse[2] ,materials[i].Opacity };
				float specular[] = { materials[i].specular[0] , materials[i].specular[1] , materials[i].specular[2] ,materials[i].Opacity };
				glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
				glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
				glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
				glMaterialf(GL_FRONT, GL_SHININESS, materials[i].SpecularWeight);
			}
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicesSplit), indicesSplit, GL_STATIC_DRAW);
		
		// render
		glBindVertexArray(*VAO);
		glBindBuffer(GL_ARRAY_BUFFER, *VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *EBO);
		glDrawElements(GL_TRIANGLES, materialPosition[k + 1] - materialPosition[k], GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		pCamera->ProcessKeyboard(FORWARD, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		pCamera->ProcessKeyboard(BACKWARD, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		pCamera->ProcessKeyboard(LEFT, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		pCamera->ProcessKeyboard(RIGHT, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_PAGE_UP) == GLFW_PRESS) {
		pCamera->ProcessKeyboard(UP, (float)deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS) {
		creste += 2 * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS) {
		if (motor<1)
			motor += 2 * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS) {
		if (motor>0.1)
			motor -= 2 * deltaTime;
		else
			motor = 0.0;
	}
	if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
		if(portbagaj<1)
			portbagaj += 2* deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS) {
		if (portbagaj>0.1)
			portbagaj -= 2 * deltaTime;
		else
			portbagaj = 0.0;
	}
	if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
		if(usaStanga<1)
			usaStanga += 2* deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS) {
		if (usaStanga > 0.1)
			usaStanga -= 2 * deltaTime;
		else
			usaStanga = 0.0;
	}
	if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
		if (usaDreapta<1)
			usaDreapta += 2 * deltaTime;
	}
	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
		if (usaDreapta > 0.1)
			usaDreapta -= 2 * deltaTime;
		else
			usaDreapta = 0.0;
	}
	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
		if (rotiPushed == false) {
			rotiPushed = true;
		}
		else
			rotiPushed = false;
	}
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
		choosedCar = 1;
		portbagaj = 0.0f;
		motor = 0.0f;
		usaStanga = 0.0f;
		usaDreapta = 0.0f;
		roti = 0.01f;
		rotiPushed = false;
	}
	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
		choosedCar = 2;
		portbagaj = 0.0f;
		motor = 0.0f;
		usaStanga = 0.0f;
		usaDreapta = 0.0f;
		roti = 0.01f;
		rotiPushed = false;
	}
	if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) {
		choosedCar = 3;
		portbagaj = 0.0f;
		motor = 0.0f;
		usaStanga = 0.0f;
		usaDreapta = 0.0f;
		roti = 0.01f;
		rotiPushed = false;
	}
	if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS) {
		if (changeGreen == 0) {
			changeGreen++;
			changeOrange = 0;
			changeDarkBlue = 0;
			changeRed = 0;
			changeViolet = 0;
			changeWhite = 0;
			changeYellow = 0;
		}
	}
	if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS) {
		if (changeOrange == 0) {
			changeOrange++;
			changeDarkBlue = 0;
			changeRed = 0;
			changeViolet = 0;
			changeWhite = 0;
			changeYellow = 0;
			changeGreen = 0;
		}
	}
	if (glfwGetKey(window, GLFW_KEY_F3) == GLFW_PRESS) {
		if (changeViolet == 0) {
			changeViolet++;
			changeOrange = 0;
			changeDarkBlue = 0;
			changeRed = 0;
			changeWhite = 0;
			changeYellow = 0;
			changeGreen = 0;
		}
	}
	if (glfwGetKey(window, GLFW_KEY_F4) == GLFW_PRESS) {
		if (changeRed == 0) {
			changeRed++;
			changeOrange = 0;
			changeDarkBlue = 0;
			changeViolet = 0;
			changeWhite = 0;
			changeYellow = 0;
			changeGreen = 0;
		}
	}
	if (glfwGetKey(window, GLFW_KEY_F5) == GLFW_PRESS) {
		if (changeWhite == 0) {
			changeWhite++;
			changeOrange = 0;
			changeDarkBlue = 0;
			changeRed = 0;
			changeViolet = 0;
			changeYellow = 0;
			changeGreen = 0;
		}
	}
	if (glfwGetKey(window, GLFW_KEY_F6) == GLFW_PRESS) {
		if (changeDarkBlue == 0) {
			changeDarkBlue++;
			changeOrange = 0;
			changeRed = 0;
			changeViolet = 0;
			changeWhite = 0;
			changeYellow = 0;
			changeGreen = 0;
		}
	}
	if (glfwGetKey(window, GLFW_KEY_F7) == GLFW_PRESS) {
		if (changeYellow == 0) {
			changeYellow++;
			changeOrange = 0;
			changeDarkBlue = 0;
			changeRed = 0;
			changeViolet = 0;
			changeWhite = 0;
			changeGreen = 0;
		}
	}
	if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS) {
		pCamera->ProcessKeyboard(DOWN, (float)deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		pCamera->Reset(width, height);

	}
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	pCamera->Reshape(width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	pCamera->MouseControl((float)xpos, (float)ypos);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yOffset)
{
	pCamera->ProcessMouseScroll((float)yOffset);
}
